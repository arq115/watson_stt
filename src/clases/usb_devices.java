/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author edwin
 */
public class usb_devices {

    private static final String USB_MOUNT_POINT = "/media/pi";

    public static String[] generar_lista() {
        String[] directories ={};
        File file = new File(USB_MOUNT_POINT);
        directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        try{
        if (directories.length == 0){
            String[] aux = {"Sin Dispositivos"};
            return aux;
        }}catch (Exception e){
             String[] aux = {"Sin Dispositivos"};
            return aux;
        }
        
        return directories;
    }

    public static void moverA(String device, JLabel estado) {
        File source = new File(archivo.DIRECTORY_LABEL+"/");
        File dest = new File(
                USB_MOUNT_POINT+"/"+device+"/"+archivo.DIRECTORY_LABEL);
        try {
            FileUtils.copyDirectory(source, dest);
        } catch (IOException e) {
            estado.setText("No hay nada para guardar");
        }

    }

}
