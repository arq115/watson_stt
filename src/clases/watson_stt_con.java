/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;

import com.ibm.watson.developer_cloud.http.HttpMediaType;
import com.ibm.watson.developer_cloud.service.security.IamOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionResults;
import com.ibm.watson.developer_cloud.speech_to_text.v1.websocket.BaseRecognizeCallback;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;

/**
 * nblibraries.properties
 *
 * @author edwin
 */
public class watson_stt_con {

    private static TargetDataLine line;
    private static TargetDataLine line_audio;
    private static String default_apikey = "Opp-4xiXini-NhN5_PoL6ypqolrmu6MphO2ehXftQ5wH";
    private static String default_url = "https://stream.watsonplatform.net/speech-to-text/api";
    private static int default_sampleRate = 16000;

    public static void iniciarReconocimiento(JTextArea text_area, JLabel estado, JToggleButton btn_encendido) throws Exception {
        archivo.crearNuevo();
        String apikey = archivo.readSetting("apikey.txt");
        String url = archivo.readSetting("url.txt");
        int sampleRate = Integer.parseInt(archivo.readSetting("freq.txt"));

        if (apikey.equals("") || url.equals("") || sampleRate == 0) {
            apikey = default_apikey;
            url = default_url;
            sampleRate = default_sampleRate;
        }

        IamOptions aut = new IamOptions.Builder()
                .apiKey(apikey)
                .build();
        SpeechToText service = new SpeechToText(aut);
        service.setEndPoint(url);

        // Signed PCM AudioFormat with 16kHz, 16 bit sample size, mono
        AudioFormat format = new AudioFormat(sampleRate, 16, 1, true, false);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

        if (!AudioSystem.isLineSupported(info)) {
            System.out.println("Line not supported");
            System.exit(0);
        }

        line = (TargetDataLine) AudioSystem.getLine(info);
        line.open(format);
        line.start();

        AudioInputStream audio = new AudioInputStream(line);

        RecognizeOptions options = new RecognizeOptions.Builder()
                .model(RecognizeOptions.Model.ES_ES_NARROWBANDMODEL)
                .audio(audio)
                .interimResults(true)
                .timestamps(true)
                .wordConfidence(true)
                //.inactivityTimeout(5) // use this to stop listening when the speaker pauses, i.e. for 5s
                .contentType(HttpMediaType.AUDIO_RAW + ";rate=" + sampleRate)
                .build();

        Thread hilo = new Thread(new Runnable() {
            public void run() {
                try {
                    service.recognizeUsingWebSocket(options, new BaseRecognizeCallback() {
                        @Override
                        public void onTranscription(SpeechRecognitionResults speechResults) {
                            try {
                                if (speechResults.getResults().get(0).isFinalResults()) {
                                    String texto = speechResults.getResults().get(0).getAlternatives().get(0).getTranscript();
                                    text_area.append(texto);
                                    // Agregar codigo para guardar en archivo
                                    archivo.agregarTexto(texto);
                                }
                            } catch (Exception e) {
                                terminar();
                                archivo.cerrar();
                            }
                        }

                        @Override
                        public void onConnected() {
                            estado.setText("Conectado.");
                            btn_encendido.setEnabled(true);
                        }

                        @Override
                        public void onDisconnected() {
                            estado.setText("Desconectado.");
                            btn_encendido.setEnabled(true);
                            archivo.cerrar();
                        }

                    });
                } catch (Exception e) {
                    estado.setText("Fallo al Conectar.");
                    btn_encendido.setEnabled(true);
                    btn_encendido.setText("Apagado");
                    btn_encendido.setBackground(Color.RED);
                    btn_encendido.setSelected(false);
                }
            }
        });
        hilo.start();

        /**
         *
         */
        /* Thread hilo1 = new Thread(new Runnable() {
            public void run() {
                try {
                    line_audio = (TargetDataLine) AudioSystem.getLine(info);
                    line_audio.open(format);
                    line_audio.start();

                    AudioInputStream audio1 = new AudioInputStream(line);
                    AudioSystem.write(audio1, AudioFileFormat.Type.WAVE, new File(
                            archivo.DIRECTORY_LABEL+"/clase_audio_" + archivo.getFecha() + ".wav"));
                } catch (IOException ex) {
                    Logger.getLogger(watson_stt_con.class.getName()).log(Level.SEVERE, null, ex);
                } catch (LineUnavailableException ex) {
                    Logger.getLogger(watson_stt_con.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        hilo1.start(); */
    }

    public static void terminar() {
        line.stop();
        line.close();
        //line_audio.stop();
        //line_audio.close();

    }

}
