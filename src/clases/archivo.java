/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class archivo {

    private static BufferedWriter writer;
    public static final String DIRECTORY_LABEL = "clases";

    public static String getFecha() {
        DateFormat formato = new SimpleDateFormat("dd-MM-yyyy_HH_mm");
        Date fecha = new Date();
        return formato.format(fecha);
    }

    public static void crearNuevo() throws IOException {
        File file = new File(DIRECTORY_LABEL + "/clase_texto_" + getFecha() + ".txt");
        file.getParentFile().mkdirs();
        writer = new BufferedWriter(new FileWriter(file));
    }

    public static void agregarTexto(String texto) throws IOException {
        writer.append(texto);
    }

    public static void cerrar() {
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void crearDefaultKey() {
    insertKey("Opp-4xiXini-NhN5_PoL6ypqolrmu6MphO2ehXftQ5wH");
    }
    
    public static void insertKey(String key) {
        BufferedWriter w = null;
        try {
            File file = new File("apikey.txt");
            w = new BufferedWriter(new FileWriter(file));
            w.append(key);
            w.close();
        } catch (IOException ex) {
            Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void crearDefaultUrl(){
    insertUrl("https://stream.watsonplatform.net/speech-to-text/api");
    }
    
    public static void insertUrl(String url) {
        BufferedWriter w = null;
        try {
            File file = new File("url.txt");
            w = new BufferedWriter(new FileWriter(file));
            w.append(url);
            w.close();
        } catch (IOException ex) {
            Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void crearDefaultFrequency(){
    InsertFrequency("8000");
    }
    
    public static void InsertFrequency(String freq) {
        BufferedWriter w = null;
        try {
            File file = new File("freq.txt");
            w = new BufferedWriter(new FileWriter(file));
            w.append(freq);
            w.close();
        } catch (IOException ex) {
            Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                w.close();
            } catch (IOException ex) {
                Logger.getLogger(archivo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static String readSetting(String archivo){
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(archivo);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            String valor = bufferedReader.readLine();
           
            // Always close files.
            bufferedReader.close();    
            return valor;
        }
        catch(FileNotFoundException ex) {
              
        }
        catch(IOException ex) {

        }
        if (archivo.equals("freq.txt"))
            return "0";
        else
            return "";
    }
}
